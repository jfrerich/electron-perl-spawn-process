"use strict";

let os = require("os");
let platform = os.platform();

let path;

if (platform !== "win32") {
  path = require("path").posix;
} else {
  path = require("path").win32;
}


// This example runs multiple processes asynchronously and shows the output of
// the processes in the Developer Tools console
//
// included node methods are spawn, exec
//
// camel-harness is an npm package that I also found.  It is used to run a perl
// process and render the output to the index.html
//
// documentation on nodejs child spawn and exec methods
// https://nodejs.org/api/child_process.html#child_process_asynchronous_process_creation
//
// camel-harness documentation
// https://www.npmjs.com/package/camel-harness


// use spawn method to run pwd shell command
// and print output to console.log
const { spawn } = require('child_process');
// spawn pwd child process
const pwd = spawn('pwd')
// print pwd stdout to console
pwd.stdout.on('data', (data) => {
  console.log(`pwd stdout:\n${data}`);
});



// Spawn two perl processes in parallel
// Output is logged to console.log to see they are running in parallel
// Spawn Process 1
let perl = spawn('perl', [path.join(__dirname, 'perl/counter.pl')])
perl.stdout.on('data', (data) => {
  console.log(`SPAWN 1 -- perl stdout:\n${data}`);
});

// Spawn Process 1
let perl2 = spawn('perl', [path.join(__dirname, 'perl/counter.pl')])
perl.stdout.on('data', (data) => {
  console.log(`SPAWN 2 -- perl2 stdout:\n${data}`);
});


const { exec } = require('child_process');


// use exec method to execute 'node --version' command
exec('node --version', (err,stdout) => {
  if (err) {
    console.error(`exec error: ${err}`);
    return;
  }
  console.log(`EXEC node version ${stdout}`);
});


// use exec method to execute perl command with 1 argument
var perl_run = path.join('perl ' , __dirname, 'perl/counter.pl 3');
    // console.log("perl_run", perl_run);
exec(perl_run, (err,stdout) => {
  if (err) {
    console.error(`exec error: ${err}`);
    return;
  }
  console.log(`EXEC -- Perl stdout ${stdout}`);
});


// camel-harness demo for Electron

// Load the camel-harness package:
const camelHarness = require("camel-harness");

// version.pl:
let versionScriptFullPath =
    path.join(__dirname, "perl", "version.pl");
console.log("versionScriptFullPath =", versionScriptFullPath);

let versionScriptObject = {};
versionScriptObject.interpreter = "perl";
versionScriptObject.script = versionScriptFullPath;
versionScriptObject.stdoutFunction = function(stdout) {
  document.getElementById("version-script").textContent = stdout;
};

// counter.pl full path:
let counterScriptFullPath =
    path.join(__dirname, "perl", "counter.pl");
console.log("counterScriptFullPath =", counterScriptFullPath);

// counter.pl - first instance:
//  use camel-harness to get output of counter.pl and print to
//  long-running-script-one HTML id
let counterOneObject = {};
counterOneObject.interpreter = "perl";
counterOneObject.script = counterScriptFullPath;
counterOneObject.stdoutFunction = function(stdout) {
  document.getElementById("long-running-script-one").textContent = stdout;
};

// counter.pl - second instance:
// Passes two argument to perl script
//  use camel-harness to get output of counter.pl and print to
//  long-running-script-two HTML id
let counterTwoObject = {};
counterTwoObject.interpreter = "perl";
counterTwoObject.script = counterScriptFullPath;
counterTwoObject.scriptArguments = [];
counterTwoObject.scriptArguments.push("5");
counterTwoObject.stdoutFunction = function(stdout) {
  document.getElementById("long-running-script-two").textContent = stdout;
};

// interactive script:
let interactiveScriptObject = {};

function startInteractiveScript() {
  let interactiveScriptFullPath =
      path.join(__dirname, "perl", "interactive.pl");
    console.log("interactiveScriptFullPath =", interactiveScriptFullPath);

  interactiveScriptObject.interpreter = "perl";
  interactiveScriptObject.script = interactiveScriptFullPath;
  
  interactiveScriptObject.stdoutFunction = function(stdout) {
    if (stdout.match(/_closed_/)) {
      const {ipcRenderer} = require("electron");
      ipcRenderer.send("asynchronous-message", "close");
    } else {
      document.getElementById("interactive-script-output").textContent = stdout;
    }
  };
  console.log("interactiveScriptObject =", interactiveScriptObject);

  camelHarness.startScript(interactiveScriptObject);
}

function sendDataToInteractiveScript() {
  let data = document.getElementById("interactive-script-input").value;
  interactiveScriptObject.scriptHandler.stdin.write(`${data}\n`);
}

function closeInteractiveScript() {
  interactiveScriptObject.scriptHandler.stdin.write("_close_\n");
}

// Wait for close event message from the main process and react accordingly:
require("electron").ipcRenderer.on("closeInteractiveScript", function() {
  closeInteractiveScript();
});
