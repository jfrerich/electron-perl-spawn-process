#!/usr/bin/perl

use strict;
use warnings;

# Built-in Perl buffering should be disabled on Windows to prevent
# seeing all STDOUT output after the script has ended.
$|=1;

my $maximal_time = 6; # seconds

my $args;
if (@ARGV) {
	$maximal_time = $ARGV[0];
	$args = "argv[0] = $ARGV[0]";
}

print "Long running Perl script started.\n";

for (my $counter=1; $counter <= $maximal_time; $counter++){
  sleep (1);
  if ($counter == 1) {
    print "$args 1 second elapsed.\n";
  }

  if ($counter > 1 and $counter <= $maximal_time) {
    print "$args $counter seconds elapsed.\n";
  }
}

sleep (1);
print "$args Long running Perl script ended.\n";
